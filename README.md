# dzr_model_icr: Computation of Infection-to-Case ratio 

Based on mortality given by research, reported deaths and demographic data, computes the expected (as well as confidence interval upper and lower bounds) infection-to-case ratio (ICR).

## Setup
### Environment variables
Set the following variables (e.g. via dotenv):
* DB_USER
* DB_PASSWORD
* DB_HOST
* DB_PORT
* DB_NAME
* DB_SSL_CERT (can be empty)
* DB_SSL_KEY (can be empty)