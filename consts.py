import os
from datetime import datetime

"""File system info"""
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

"""DB Table names"""
CASE_TABLE = "SOR_FACT_CASE"
CASES_VIEW_COUNTY = "DM_MDL_PRM_COUNTY"
CASES_VIEW_STATE = "DM_MDL_PRM_STATE"
CASES_VIEW_COUNTRY = "DM_MDL_PRM_COUNTRY"
DIM_COUNTY_TABLE = "SOR_DIM_COUNTY"
"""DB Constants"""
TARGET_COL = "TARGET_ID"
"""Data constants"""
TIME_TO_DEATH_AVG = 18
DEFAULT_START_DATE = datetime.strptime("2020-03-09", "%Y-%m-%d")
DEFAULT_IFR_BASE = 0.00358  # Taken from Heinsberg Study
TARGET_LEVELS = ["COUNTY", "STATE", "COUNTRY"]
