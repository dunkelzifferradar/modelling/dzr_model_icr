import logging

import numpy as np
from scipy.stats import binom, nbinom

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class InfectionCasesRatioEstimator:
    def __init__(self, deaths, cases, f_rate):
        """
        Calculates ratio of virus estimated virus infections to discovered cases from testing.
        Estimation is based on fatality rate parameter taken from virus studies on a broader sample

        Args:
            deaths (nd.array):  Deaths per region
            cases (nd.array): Cases per region
            f_rate (float): fatality rate from study
        """
        assert deaths.shape[0] == cases.shape[0], "Equal amount of elements must be provided for deaths and cases"
        self.deaths = deaths
        self.cases = cases
        self.f_rate = f_rate
        self.ci_level = None
        self.zero_death_infections_upper = None

    def zero_deaths_mu(self):
        # With a probability of 99.99% amount of infected people if there is one death must be at least
        self.zero_death_infections_upper = min(nbinom.interval(0.0001, 1, self.f_rate))
        # Thus we can assume that upper bound of infected people for
        # 0 deaths should be slightly lower than lowest amount for 1 deaths
        self.zero_death_infections_upper -= 1

    def prepare_data(self):
        # transform arrays if not multidimensional
        if np.ndim(self.deaths) == 1:
            self.deaths = self.deaths.reshape(-1, 1)
        if np.ndim(self.cases) == 1:
            self.cases = self.cases.reshape(-1, 1)

    def infections_ci_calc(self, infections_mu_ci_calc):
        # Calculate lower and upper bounds for deaths
        deaths_lower, deaths_upper = binom.interval(self.ci_level, infections_mu_ci_calc, self.f_rate)
        logger.debug(f'deaths_lower: {deaths_lower}')
        logger.debug(f'deaths_upper: {deaths_upper}')

        # Calculate lower and upper bounds for infections
        infections_lower, infections_upper = deaths_lower / self.f_rate, deaths_upper / self.f_rate

        # Replace lower and upper infection bound for zero deaths and to low infections
        infections_lower = np.fmax(infections_lower, self.cases)  # infections not lower than cases
        # Replace nan values for upper bound with estimated maximum upper bound for zero deaths
        infections_upper = np.where(np.isnan(infections_upper), self.zero_death_infections_upper, infections_upper)
        logger.debug(f'infections_lower: {infections_lower}')
        logger.debug(f'infections_upper: {infections_upper}')
        return infections_lower, infections_upper

    def infection_cases_ratio_calc(self, infections_mu, infections_lower, infections_upper):
        # Calculate upper and lower bounds of "Cases to Infection Ratio"
        inf_cases_ratio_upper, inf_cases_ratio_lower = self.cases / infections_lower, self.cases / infections_upper
        # Replace expected infections of zero (due to zero deaths with a mean of lower and upper bound)
        infections_ci_mean = np.mean(np.stack((infections_lower, infections_upper), axis=1), axis=1)
        infections_mu = np.where(infections_mu == 0,
                                 infections_ci_mean,
                                 infections_mu)
        # Return expected value, lower and upper bound of cases to infections ratio
        expected_infections_cases_ratio = self.cases / infections_mu
        return expected_infections_cases_ratio, inf_cases_ratio_lower, inf_cases_ratio_upper

    def binomial_ci(self, alpha):
        """
        Args:
            alpha (float):  one sided alpha for confidence level

        Returns:
            expected_cases_infections_ratio: Expected values of cases to infection ratio
            cases_inf_ratio_lower: Lower bound values of cases to infection ratio
            cases_inf_ratio_upper: Upper bound values of cases to infection ratio
        """
        # Calculate upper bound for the case there are 0 reported deaths
        self.zero_deaths_mu()
        # transform alpha to confidence level representation
        self.ci_level = 1 - alpha / 2
        # Prepare data
        self.prepare_data()
        # Calculate Expected value for infections
        infections_mu = self.deaths / self.f_rate
        # Replace expected value of infections_mu by zero for confidence interval calculation
        infections_mu_ci = np.nan_to_num(infections_mu).astype(np.int32)
        # Calculate lower and upper bounds of infections
        infections_lower, infections_upper = self.infections_ci_calc(infections_mu_ci)
        # Calculate and return expected, lower and upper infections to cases ratio
        return self.infection_cases_ratio_calc(infections_mu, infections_lower, infections_upper)
