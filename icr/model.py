import logging
import pprint
from datetime import datetime, date, timedelta
from typing import Union

import numpy as np
import pandas as pd
from dateutil.utils import today
from dotenv import load_dotenv
from dzr_shared_util.db.db_conn import DBConnector
from pandas import DataFrame
from statsmodels.tsa import holtwinters
from statsmodels.tsa.api import SimpleExpSmoothing

from consts import (CASES_VIEW_STATE, DIM_COUNTY_TABLE, TIME_TO_DEATH_AVG, DEFAULT_START_DATE,
                    DEFAULT_IFR_BASE, ROOT_DIR, CASES_VIEW_COUNTY, TARGET_COL, CASES_VIEW_COUNTRY, TARGET_LEVELS)
from icr.InfectionCaseRatioEstimation import InfectionCasesRatioEstimator

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class ICRRunner:
    db: DBConnector

    def __init__(self, db_conn) -> None:
        self.db = db_conn

    def _get_reported_info(self, target_level: str, target_id: Union[int, list], start_date: date, end_date: date,
                           geo_aggr: bool) -> DataFrame:
        # date range inclusive on both ends
        if start_date is None:
            start_date = DEFAULT_START_DATE
        # if end_date is not None:
        #    end_death_date = end_date - timedelta(TIME_TO_DEATH_AVG)
        if end_date is None:
            # end_death_date
            end_date = today()  # - timedelta(TIME_TO_DEATH_AVG)
        # always make target id into a list of ids
        if isinstance(target_id, int):
            target_ids = [target_id]
        elif isinstance(target_id, list) and [isinstance(x, int) for x in target_id]:
            target_ids = target_id
        else:
            raise TypeError("Target ID must be integer or list of integers")
        # set up params based on target level
        assert target_level in TARGET_LEVELS, "Invalid target level provided: " + str(target_level)
        query_target_ids = target_ids.copy()
        if target_level == 'COUNTY':
            if geo_aggr:
                case_source = CASES_VIEW_STATE
                query_target_ids = self._aggregate_counties_to_state(target_ids)
            else:
                case_source = CASES_VIEW_COUNTY
        elif target_level == 'STATE':
            case_source = CASES_VIEW_STATE
        else:
            case_source = CASES_VIEW_COUNTRY
        # build query and get data
        where_str = self._build_where_query(end_date, start_date, query_target_ids)
        df: DataFrame = self.db.get_data(case_source, where=where_str).sort_values(by=[TARGET_COL, "REPORT_DATE"])
        if df.empty:
            raise ValueError("Case data for provided target id(s) " + str(target_id) + " for level " + str(
                target_level) + " was empty.")
        # Calculate Estimate Time of Death
        # df["DEATH_DATE"] = df["REPORT_DATE"].apply(self._compute_death_date)
        # Fill forward/backward/zero data for missing data entries
        df = self._fill_timeseries_nas(df, end_date, geo_aggr, start_date, target_ids)
        df.index.name = "REPORT_DATE"
        df.reset_index(inplace=True)
        # Recalculate Estimate Time of Death
        # df["DEATH_DATE"] = df["REPORT_DATE"].apply(lambda report_date: self._compute_death_date(report_date))
        return df

    def _build_where_query(self, end_date, start_date, target_id):
        where_str = TARGET_COL + " in (" + ", ".join([str(x) for x in target_id]) + ")"
        # start_death_date = start_date - timedelta(TIME_TO_DEATH_AVG)
        where_str += " and REPORT_DATE >= '" + str(start_date) + "'"  # start_death_date) + "'"
        where_str += " and REPORT_DATE <= '" + str(end_date) + "'"  # end_death_date) + "'"
        return where_str

    def _aggregate_counties_to_state(self, target_ids):
        # Create County State mapping and replace county_ids with state_ids
        df_county_state: DataFrame = self.db.get_data(DIM_COUNTY_TABLE,
                                                      where="ID in (" + ", ".join([str(id) for id in target_ids]) + ")")
        df_county_state.rename(columns={"ID": "COUNTY_ID"}, inplace=True)
        self.county_state_dict = df_county_state[["COUNTY_ID", "STATE_ID"]].drop_duplicates().set_index(
            "COUNTY_ID").to_dict()["STATE_ID"]
        # Map counties to states
        target_id_mapped = []
        for target_id in target_ids:
            target_id_mapped.append(self.county_state_dict[target_id])
        target_id_mapped = list(set(target_id_mapped))
        return target_id_mapped

    def _fill_timeseries_nas(self, df, end_date, geo_aggr, start_date, target_ids):
        df_temp = None
        for target_id in target_ids:
            try:
                query_target_id = target_id
                if geo_aggr and hasattr(self, "county_state_dict"):
                    query_target_id = self.county_state_dict[target_id]
                df_temp_source = df.query("TARGET_ID == " + str(query_target_id)).set_index("REPORT_DATE")
                if df_temp_source.empty:
                    raise ValueError("Case data for one of the target ids (" + str(target_id) + ") was empty.")
                data_range_index = pd.date_range(start_date, end_date, freq='D')
                df_temp_filled = pd.DataFrame([],
                                              index=data_range_index
                                              ) \
                    .join(df_temp_source, how='left').copy()
                df_temp_filled.fillna({"CASE_COUNT": 0, "DECEASED_COUNT": 0}, inplace=True)
                df_temp_filled = df_temp_filled.fillna(method='ffill').fillna(method='bfill')
                if geo_aggr:
                    df_temp_filled["TARGET_ID"].replace(query_target_id, target_id, inplace=True)
                if df_temp is None:
                    df_temp = df_temp_filled
                else:
                    df_temp = pd.concat((df_temp, df_temp_filled))
            except Exception as e:
                logger.exception(e)
        df = df_temp.astype({
            "TARGET_ID": int
        })
        df_temp = df_temp_filled = df_temp_source = pd.DataFrame()  # Free up memory
        return df

    # def _compute_death_date(self, report_date):
    #    return report_date + timedelta(TIME_TO_DEATH_AVG)

    def evaluate(self, input_json):
        if "START_DATE" in input_json:
            start_date = datetime.strptime(input_json["START_DATE"], "%Y-%m-%d")
        else:
            start_date = None
        if "END_DATE" in input_json:
            end_date = datetime.strptime(input_json["END_DATE"], "%Y-%m-%d")
        else:
            end_date = today().date()
        if "DAYS_AGGR" in input_json:
            days_aggr = input_json["DAYS_AGGR"]
        else:
            days_aggr = 1
        if "GEO_AGGR" in input_json:
            geo_aggr = bool(input_json["GEO_AGGR"])
        else:
            geo_aggr = False
        if "SMOOTHING" in input_json:
            smoothing = input_json["SMOOTHING"]
        else:
            smoothing = None

        icr_df = self.estimate_icr(input_json["TARGET_ID"], input_json["TARGET_LEVEL"], start_date, end_date,
                                   days_aggr, geo_aggr, input_json["IFR_BASE"], smoothing)
        icr_df = icr_df[["TARGET_ID", "ICR_CI_LOWER", "ICR_CI_UPPER", "ICR_EXP_VAL"]].copy()
        icr_df.index.name = "DATE"
        return icr_df.to_json(orient='table')

    def estimate_icr(self, target_id: Union[int, list],
                     target_level: str = "COUNTY",
                     start_date: date = None,
                     end_date: date = today().date(),
                     days_aggr: int = 1,
                     geo_aggr=False,
                     research_base: float = DEFAULT_IFR_BASE,
                     smoothing: str = None) -> DataFrame:
        assert target_level in ["COUNTY", 'STATE', 'COUNTRY'], "Level must be either County, State or Country!"
        # delete computed values for last <average time till death plus some more to be safe> days
        #  and fill with values before
        if isinstance(start_date, datetime):
            start_date = start_date.date()
        if isinstance(end_date, datetime):
            end_date = end_date.date()
        last_date_of_reliable_death_data = min(end_date, today().date() - timedelta(TIME_TO_DEATH_AVG + 3))
        df_cases_deaths = self._get_reported_info(target_level, target_id, start_date, end_date, geo_aggr)
        if "TARGET_ID" not in df_cases_deaths.columns:
            df_cases_deaths["TARGET_ID"] = target_id
        # Iterate through targets and calculate icr, finally glue data together
        df_calc = None
        for target_id in df_cases_deaths["TARGET_ID"].unique():
            try:
                # Calculate on a copy dew to further transformations
                df_temp_source = df_cases_deaths.query("TARGET_ID == " + str(target_id)).copy()
                df_cases_deaths_icr_calc = df_temp_source.drop("TARGET_ID", axis=1).copy()
                if days_aggr > 1:
                    # Reformat DEATH_DATE to Datetime
                    df_cases_deaths_icr_calc["REPORT_DATE"] = pd.to_datetime(df_cases_deaths_icr_calc["REPORT_DATE"])
                    # Aggregate
                    df_cases_deaths_icr_calc = df_cases_deaths_icr_calc.resample(str(days_aggr) + 'D', on="REPORT_DATE") \
                        .sum().reset_index().copy()
                elif days_aggr <= 0:
                    raise ValueError("Only positive aggregation days")
                df_temp_source.set_index(["REPORT_DATE"], inplace=True, drop=False)
                df_cases_deaths_icr_calc.set_index(["REPORT_DATE"], inplace=True)

                cases = df_cases_deaths_icr_calc["CASE_COUNT"]
                deaths = df_cases_deaths_icr_calc["DECEASED_COUNT"]
                icr = InfectionCasesRatioEstimator(np.array([deaths]), np.array([cases]),
                                                   research_base)
                icr_exp, icr_lower, icr_upper = icr.binomial_ci(alpha=0.1)
                if smoothing is not None:
                    icr_exp = self.icr_smooth_transform(icr_exp, smoothing)
                    icr_lower = self.icr_smooth_transform(icr_lower, smoothing)
                    icr_upper = self.icr_smooth_transform(icr_upper, smoothing)
                else:
                    icr_exp = np.fmin(icr_exp, 1).reshape(1, -1)
                    icr_lower = np.fmin(icr_lower, 1).reshape(1, -1)
                    icr_upper = np.fmin(icr_upper, 1).reshape(1, -1)
                icr_df = pd.DataFrame(np.concatenate((icr_exp, icr_lower, icr_upper), axis=0).transpose(),
                                      columns=("ICR_EXP_VAL", "ICR_CI_LOWER", "ICR_CI_UPPER"),
                                      index=df_cases_deaths_icr_calc.index)
                # add to (existing) output df
                df_target_icr = df_temp_source.join(icr_df, how='left').fillna(method='ffill')
                # fill up computed dataframe with values until the input end date - due to "time till death", the last bit
                #  is filled up according to values before.
                if last_date_of_reliable_death_data < end_date:
                    last_datetime_of_reliable_death_data = datetime(last_date_of_reliable_death_data.year,
                                                                    last_date_of_reliable_death_data.month,
                                                                    last_date_of_reliable_death_data.day)
                    df_before = df_target_icr[df_target_icr["REPORT_DATE"] <= last_datetime_of_reliable_death_data].copy()
                    df_last_k_days = df_target_icr[
                        df_target_icr["REPORT_DATE"] > last_datetime_of_reliable_death_data].copy()
                    for numeric_col in ["ICR_EXP_VAL", "ICR_CI_LOWER", "ICR_CI_UPPER"]:
                        df_last_k_days[numeric_col] = np.nan
                    df_target_icr = df_before.append(df_last_k_days)
                    for numeric_col in ["ICR_EXP_VAL", "ICR_CI_LOWER", "ICR_CI_UPPER"]:
                        df_target_icr[numeric_col].fillna(method='ffill', inplace=True)

                if df_calc is None:
                    df_calc = df_target_icr.copy()
                else:
                    df_calc = pd.concat((df_calc, df_target_icr))
            except Exception as e:
                logger.exception(e)
        return df_calc

    def smooth(self, time_series, method):
        if method == "seasonal":
            model = holtwinters.ExponentialSmoothing(time_series, seasonal='add', seasonal_periods=7)
        elif method == "exponential":
            model = SimpleExpSmoothing(time_series)
        else:
            raise Exception("Implemented methods are: 'seasonal', 'exponential'")
        result = model.fit()
        return result.fittedvalues

    def icr_smooth_transform(self, time_series, method):
        return np.fmin(np.fmax(self.smooth(time_series.ravel(), method), min(time_series)), 1).reshape(1, -1)


# ICRRunner Evaluate wrapper function
def icr_eval_wrapper(body):
    db_connection = DBConnector()
    icr_json = {}
    try:
        icr_runner = ICRRunner(db_connection)
        icr_json = icr_runner.evaluate(body)
    except Exception as e:
        logger.exception(e)
    finally:
        db_connection.close_connection()
    return icr_json


if __name__ == '__main__':
    load_dotenv(ROOT_DIR + "/.env")
    json_res = icr_eval_wrapper({"TARGET_ID": [4011, 4012, 928372], "TARGET_LEVEL": "COUNTY", "IFR_BASE": 0.00358})
    print(json_res)
    """db_conn = DBConnector()
    pp = pprint.PrettyPrinter(indent=4)
    try:
        runner = ICRRunner(db_conn)
        # county_id = 1001
        county_id = [1001, 6413, 11000]
        icrs = runner.estimate_icr(target_id=county_id, days_aggr=1, geo_aggr=True)#, smoothing="seasonal")
        print("County id: " + str(county_id))
        pp.pprint(icrs)
        # for dt, vals in icrs.items():
        # print(str(dt) + ": (" + ", ".join([str(x[0]) for x in vals.values()]) + ")")

    finally:
        db_conn.close_connection()"""
