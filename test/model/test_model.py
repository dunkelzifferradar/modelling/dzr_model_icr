import unittest
from datetime import date, datetime, timedelta
from functools import reduce
from typing import Union
from unittest.mock import patch

from dotenv import load_dotenv
from dzr_shared_util.db.db_conn import DBConnector
from pandas import DataFrame, read_csv
from pandas._testing import assert_is_sorted

from consts import ROOT_DIR, TIME_TO_DEATH_AVG
from icr.model import ICRRunner

load_dotenv(ROOT_DIR + "/.env")

MAX_DAYS_TEST = 100


# TODO: Add test for geo_aggr
def mock_get_reported_info(target_level, target_id: Union[int, list], start_date: date, end_date: date,
                           geo_aggr: bool) -> DataFrame:
    if start_date is None:
        start_date = end_date - timedelta(MAX_DAYS_TEST)

    n = (end_date - start_date).days + 1
    num_range = range(0, n)
    # death_dates = [start_date + timedelta(i) for i in num_range]
    report_dates = [start_date + timedelta(i) for i in num_range]
    vals = {
        'REPORT_DATE': [datetime(d.year, d.month, d.day) for d in report_dates],
        'CASE_COUNT': [20] * n,
        'DECEASED_COUNT': [2] * n
    }
    if isinstance(target_id, list):
        for key, val in vals.items():
            vals[key] = val * len(target_id)
        vals["TARGET_ID"] = reduce(lambda x, y: x + y, [[id] * n for id in target_id])
    df = DataFrame(vals)
    return df


def mock_get_data(tbl_name, where=""):
    where = where.replace("=", "==").replace(">==", ">=").replace("<==", "<=").replace("(", "[").replace(")", "]")
    samples_df = read_csv(ROOT_DIR + '/test/data/' + tbl_name + '.csv', sep=',', parse_dates=["REPORT_DATE"])
    df_where_res = samples_df.query(where).rename(columns={"REPORT_DATE": "REP_DATE_OLD"})
    df_where_res["REPORT_DATE"] = df_where_res["REP_DATE_OLD"].dt.date
    return df_where_res.drop(columns=["REP_DATE_OLD"])


def mock_today():
    samples_df: DataFrame = read_csv(ROOT_DIR + '/test/data/DM_MDL_PRM_COUNTY.csv', sep=',', parse_dates=["REPORT_DATE"])
    max_date = samples_df["REPORT_DATE"].max()
    return max_date


class TestSomeModel(unittest.TestCase):

    @patch('dzr_shared_util.db.db_conn.DBConnector.__init__', return_value=None)
    def setUp(self, mock_init):
        self.db = DBConnector()
        self.cut = ICRRunner(self.db)

    @patch('dzr_shared_util.db.db_conn.DBConnector.get_data', side_effect=mock_get_data)
    def test_get_reported_info_single_county(self, mock_get_data_method):
        county_id = 1001
        start_date = datetime.strptime('2020-04-01', "%Y-%m-%d").date()
        end_date = datetime.strptime('2020-04-30', "%Y-%m-%d").date()
        rep_info = self.cut._get_reported_info("COUNTY", county_id, start_date, end_date, False)
        # Test number of return entries based on the start and end date
        self.assertEqual(((end_date - start_date).days + 1), len(rep_info.index))
        exp_cols = ['TARGET_ID', 'REPORT_DATE', 'CASE_COUNT', 'DECEASED_COUNT']
        self.assertEqual(set(exp_cols), set(rep_info.columns))
        # make sure that entries are sorted by date
        assert_is_sorted(rep_info["REPORT_DATE"])
        df_case_input = mock_get_data("DM_MDL_PRM_COUNTY", "TARGET_ID = " + str(
            county_id) + " and REPORT_DATE >= '" + str(start_date) + "' and REPORT_DATE <= '" + str(end_date) + "'")
        # make sure that sum of cases does not change during processing
        self.assertEqual(df_case_input["CASE_COUNT"].sum(), rep_info["CASE_COUNT"].sum())

    @patch('dzr_shared_util.db.db_conn.DBConnector.get_data', side_effect=mock_get_data)
    def test_get_reported_info_mult_counties(self, mock_get_data_method):
        county_ids = [1001, 1002]
        start_date = datetime.strptime('2020-04-01', "%Y-%m-%d").date()
        end_date = datetime.strptime('2020-04-30', "%Y-%m-%d").date()
        rep_info = self.cut._get_reported_info("COUNTY", county_ids, start_date, end_date, False)
        # Test number of return entries based on the start and end date and amount of individual counties
        self.assertEqual(((end_date - start_date).days + 1) * len(county_ids), len(rep_info.index))
        # make sure output has correct columns
        exp_cols = ['TARGET_ID', 'REPORT_DATE', 'CASE_COUNT', 'DECEASED_COUNT']
        self.assertEqual(set(exp_cols), set(rep_info.columns))
        # make sure all values are associated to one of the given county ids
        self.assertEqual(county_ids, rep_info["TARGET_ID"].unique().tolist())
        for county_id in county_ids:
            target_rep_info = rep_info[rep_info["TARGET_ID"] == county_id]
            # make sure that entries are sorted by date
            assert_is_sorted(target_rep_info["REPORT_DATE"])
            df_case_input = mock_get_data("DM_MDL_PRM_COUNTY", "TARGET_ID = " + str(
                county_id) + " and REPORT_DATE >= '" + str(start_date) + "' and REPORT_DATE <= '" + str(end_date) + "'")
            # make sure that sum of cases does not change during processing
            self.assertEqual(df_case_input["CASE_COUNT"].sum(), target_rep_info["CASE_COUNT"].sum())

    @patch('dzr_shared_util.db.db_conn.DBConnector.get_data', side_effect=mock_get_data)
    def test_get_reported_info_single_state(self, mock_get_data_method):
        state_id = 13
        start_date = datetime.strptime('2020-04-01', "%Y-%m-%d").date()
        end_date = datetime.strptime('2020-04-30', "%Y-%m-%d").date()
        rep_info = self.cut._get_reported_info("STATE", state_id, start_date, end_date, False)
        # Test number of return entries based on the start and end date
        self.assertEqual(((end_date - start_date).days + 1), len(rep_info.index))
        exp_cols = ['TARGET_ID', 'REPORT_DATE', 'CASE_COUNT', 'DECEASED_COUNT']
        self.assertEqual(set(exp_cols), set(rep_info.columns))
        # make sure that entries are sorted by date
        assert_is_sorted(rep_info["REPORT_DATE"])
        df_case_input = mock_get_data("DM_MDL_PRM_STATE", "TARGET_ID = " + str(
            state_id) + " and REPORT_DATE >= '" + str(start_date) + "' and REPORT_DATE <= '" + str(end_date) + "'")
        # make sure that sum of cases does not change during processing
        self.assertEqual(df_case_input["CASE_COUNT"].sum(), rep_info["CASE_COUNT"].sum())

    @patch('dzr_shared_util.db.db_conn.DBConnector.get_data', side_effect=mock_get_data)
    def test_get_reported_info_single_country(self, mock_get_data_method):
        state_id = 0
        start_date = datetime.strptime('2020-04-01', "%Y-%m-%d").date()
        end_date = datetime.strptime('2020-04-30', "%Y-%m-%d").date()
        rep_info = self.cut._get_reported_info("COUNTRY", state_id, start_date, end_date, False)
        # Test number of return entries based on the start and end date
        self.assertEqual(((end_date - start_date).days + 1), len(rep_info.index))
        exp_cols = ['TARGET_ID', 'REPORT_DATE', 'CASE_COUNT', 'DECEASED_COUNT']
        self.assertEqual(set(exp_cols), set(rep_info.columns))
        # make sure that entries are sorted by date
        assert_is_sorted(rep_info["REPORT_DATE"])
        df_case_input = mock_get_data("DM_MDL_PRM_COUNTRY", "TARGET_ID = " + str(
            state_id) + " and REPORT_DATE >= '" + str(start_date) + "' and REPORT_DATE <= '" + str(end_date) + "'")
        # make sure that sum of cases does not change during processing
        self.assertEqual(df_case_input["CASE_COUNT"].sum(), rep_info["CASE_COUNT"].sum())

    @patch('icr.model.ICRRunner._get_reported_info', side_effect=mock_get_reported_info)
    def test_estimate_icr_single_county(self, mocked_get_reported_info_method):
        county_id = 1001
        icrs = self.cut.estimate_icr(target_id=county_id, target_level="COUNTY")
        self.assertEqual(MAX_DAYS_TEST+1, icrs.shape[0])
        self.assert_output_fields(icrs)
        self.assert_output_vals(icrs, [county_id])

    @patch('icr.model.ICRRunner._get_reported_info', side_effect=mock_get_reported_info)
    def test_estimate_icr_single_state(self, mocked_get_reported_info_method):
        state_id = 14
        icrs = self.cut.estimate_icr(target_id=state_id, target_level="STATE")
        self.assertEqual(MAX_DAYS_TEST + 1, icrs.shape[0])
        self.assert_output_fields(icrs)
        self.assert_output_vals(icrs, [state_id])

    @patch('icr.model.ICRRunner._get_reported_info', side_effect=mock_get_reported_info)
    def test_estimate_icr_single_country(self, mocked_get_reported_info_method):
        country_id = 0
        icrs = self.cut.estimate_icr(target_id=country_id, target_level="COUNTRY")
        self.assertEqual(MAX_DAYS_TEST + 1, icrs.shape[0])
        self.assert_output_fields(icrs)
        self.assert_output_vals(icrs, [country_id])

    @patch('icr.model.ICRRunner._get_reported_info', side_effect=mock_get_reported_info)
    def test_estimate_icr_mult_county(self, mocked_get_reported_info_method):
        county_ids = [1001, 1002]
        icrs = self.cut.estimate_icr(target_id=county_ids, target_level="COUNTY")
        self.assertEqual((MAX_DAYS_TEST+1) * len(county_ids), icrs.shape[0])
        self.assert_output_fields(icrs)
        self.assert_output_vals(icrs, county_ids)

    @patch('icr.model.ICRRunner._get_reported_info', side_effect=mock_get_reported_info)
    def test_estimate_icr_spec_dates(self, mocked_get_reported_info_method):
        county_id = 1001
        start_date = datetime.strptime('2020-03-30', "%Y-%m-%d").date()
        end_date = datetime.strptime('2020-03-31', "%Y-%m-%d").date()
        icrs = self.cut.estimate_icr(target_id=county_id,
                                     target_level="COUNTY",
                                     start_date=start_date,
                                     end_date=end_date)
        self.assertEqual((end_date - start_date).days + 1, icrs.shape[0])
        date_range = [start_date + timedelta(i) for i in range(0, (end_date - start_date).days + 1)]
        returned_dates = [ts.date() for ts in icrs.index]
        self.assertTrue(all([day in returned_dates for day in date_range]),
                        "Values returned for dates: " + str(returned_dates))
        self.assertTrue(all([day in date_range for day in returned_dates]),
                        "Values returned for dates: " + str(returned_dates))
        self.assert_output_fields(icrs)
        self.assert_output_vals(icrs, [county_id])

    @patch('icr.model.today', side_effect=mock_today)
    @patch('dzr_shared_util.db.db_conn.DBConnector.get_data', side_effect=mock_get_data)
    def test_estimate_icr_fill_last_k_days(self, mocked_today_method, mock_get_data_method):
        county_id = 1001
        end_date = mock_today().date()
        start_date = end_date - timedelta(45)
        icrs = self.cut.estimate_icr(target_id=county_id,
                                     target_level="COUNTY",
                                     start_date=start_date,
                                     end_date=end_date)
        cols_that_should_have_same_val = ["ICR_EXP_VAL", "ICR_CI_LOWER", "ICR_CI_UPPER"]
        date_when_front_fill_should_begin: date = (end_date - timedelta(TIME_TO_DEATH_AVG + 3))
        datetime_when_front_fill_should_begin = datetime(date_when_front_fill_should_begin.year,
                                                         date_when_front_fill_should_begin.month,
                                                         date_when_front_fill_should_begin.day)
        for col in cols_that_should_have_same_val:
            col_values_front_fill = icrs[icrs["REPORT_DATE"] > datetime_when_front_fill_should_begin][col].to_list()
            # make sure there is only 1 unique value
            self.assertEqual(1, len(set(col_values_front_fill)))

    def assert_output_fields(self, icrs):
        self.assertTrue("ICR_EXP_VAL" in icrs.columns
                        and "ICR_CI_LOWER" in icrs.columns
                        and "ICR_CI_UPPER" in icrs.columns
                        and "TARGET_ID" in icrs.columns)

    def assert_output_vals(self, icrs, county_ids):
        self.assertTrue(all([x == 0.0358 for x in round(icrs["ICR_EXP_VAL"], 4)])
                        and all([x == 0.0143 for x in round(icrs["ICR_CI_LOWER"], 4)])
                             and all([x  == 1 for x in icrs["ICR_CI_UPPER"]])
                             and all([x in county_ids for x in icrs["TARGET_ID"].unique()]))

    @patch('dzr_shared_util.db.db_conn.DBConnector.close_connection', return_value=None)
    def tearDown(self, mock_close_connection_method):
        self.db.close_connection()


if __name__ == '__main__':
    unittest.main()
