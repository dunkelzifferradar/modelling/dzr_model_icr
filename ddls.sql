-- View for aggregated cases and deceased on state level
CREATE OR REPLACE VIEW public.DM_MDL_PRM_STATE as
SELECT REPORT_DATE, STATE_ID as TARGET_ID, sum(CASE_COUNT) as CASE_COUNT, sum(DECEASED_COUNT) AS DECEASED_COUNT
FROM SOR_FACT_CASE cas
JOIN SOR_DIM_COUNTY cty
ON cas.county_id = cty.id
GROUP BY REPORT_DATE, STATE_ID
ORDER BY REPORT_DATE asc;